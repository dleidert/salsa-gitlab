TODO for sals-gitlab
====================

The values of --data should be put inside a global hash %data and it should be
emptied, when %opt is emptied too. But how to handle --list-* options? Because
--list-* is executed when occuring, --data must be defined before --list-foo.
So make it a pre-requisite to specify --data always before --option-using-data
so $data{'option'} gets filled with $opt{data} as soon as --option appears.

The returned items (e.g. for listing all projects for a group) are limited
to a number of 20. This can only be raised to 100. So one needs to loop through
the results.

